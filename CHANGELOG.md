# Change Log

All notable changes to this project will be documented in this file.

## [1.0.0a2] - 2016-11-21

### Changed

- Fixed issue #9 so that ServerRequest::$xhr is computed correctly.

- Updated docs

## [1.0.0a1] - 2016-11-17

Initial release.


* * *

[1.0.0a2]: https://gitlab.com/pmjones/ext-request/tags/1.0.0a2
[1.0.0a1]: https://gitlab.com/pmjones/ext-request/tags/1.0.0a1